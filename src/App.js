import React from "react";
import MyBlockChain from "./components/MyBlockChain";

function App() {
  return (
    <div>
      <MyBlockChain />
    </div>
  );
}

export default App;

import React, { useState } from "react";
import Web3 from "web3";

function MyBlockChain() {
  const [currentBlock, setCurrentBlock] = useState("");
  const [transactionDetail, setTransactionDetail] = useState([]);

  const handelClick = () => {
    console.log("button Clicked");
    myfun();
  };

  const myfun = async () => {
    console.log("funtion Called");
    const web3 = await new Web3("https://mainnet.eth.cloud.ava.do/");
    const latest_block_number = await web3.eth.getBlockNumber();
    setCurrentBlock(latest_block_number);
    console.log(latest_block_number);
    const block_details = await web3.eth.getBlock(latest_block_number);
    console.log(block_details);
    let my_result = await web3.eth.getTransaction(
      block_details.transactions[20]
    );

    console.log(my_result);
    setTransactionDetail([my_result]);
  };
  return (
    <div>
      <button onClick={handelClick}>Click Me</button>
      <p>Current Block :{currentBlock} </p>
      <p>Transaction Details :</p>
      {transactionDetail.map((item, i) => {
        return (
          <div key={i}>
            <p>Gas - {item.gas} </p>
            <p>Gas Price - {item.gasPrice} </p>
          </div>
        );
      })}
    </div>
  );
}

export default MyBlockChain;
